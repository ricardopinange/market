CREATE TABLE "product_type" (
    "id" uuid NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "deleted_at" timestamp without time zone NULL,
    CONSTRAINT "PK_product_type" PRIMARY KEY ("id")
);

CREATE TABLE "tax" (
    "id" uuid NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "deleted_at" timestamp without time zone NULL,
    CONSTRAINT "PK_tax" PRIMARY KEY ("id")
);

CREATE TABLE "product_type_tax" (
    "id" uuid NOT NULL,
    "product_type_id" uuid NOT NULL,
	"tax_id" uuid NOT NULL,
    "percentage" numeric(5,2) NOT NULL,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "deleted_at" timestamp without time zone NULL,
    CONSTRAINT "PK_product_type_tax" PRIMARY KEY ("id"),
    CONSTRAINT "FK_product_type_tax_product_type_id" FOREIGN KEY ("product_type_id") REFERENCES "product_type" ("id"),
	CONSTRAINT "FK_product_type_tax_tax_id" FOREIGN KEY ("tax_id") REFERENCES "tax" ("id")
);

CREATE TABLE "product" (
    "id" uuid NOT NULL,
    "product_type_id" uuid NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "price" money NOT NULL,
	"description" text DEFAULT NULL,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "deleted_at" timestamp without time zone NULL,
    CONSTRAINT "PK_product" PRIMARY KEY ("id"),
    CONSTRAINT "FK_product_product_type_id" FOREIGN KEY ("product_type_id") REFERENCES "product_type" ("id")
);

CREATE TABLE "sale" (
    "id" uuid NOT NULL,
    "amount" money NOT NULL,
    "amount_tax" money NOT NULL,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "deleted_at" timestamp without time zone NULL,
    CONSTRAINT "PK_sale" PRIMARY KEY ("id")
);

CREATE TABLE "sale_product" (
    "id" uuid NOT NULL,
    "sale_id" uuid NOT NULL,
    "product_id" uuid NOT NULL,
	"quantity" int NOT NULL,
	"price" money NOT NULL,
    "amount" money NOT NULL,
    "amount_tax" money NOT NULL,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "deleted_at" timestamp without time zone NULL,
    CONSTRAINT "PK_sale_product" PRIMARY KEY ("id"),
    CONSTRAINT "FK_sale_product_sale_id" FOREIGN KEY ("sale_id") REFERENCES "sale" ("id"),
    CONSTRAINT "FK_sale_product_product_id" FOREIGN KEY ("product_id") REFERENCES "product" ("id")
);

CREATE TABLE "sale_product_tax" (
    "id" uuid NOT NULL,
    "sale_product_id" uuid NOT NULL,
    "tax_id" uuid NOT NULL,
    "percentage" numeric(5,2) NOT NULL,
    "amount" money NOT NULL,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "deleted_at" timestamp without time zone NULL,
    CONSTRAINT "PK_sale_product_tax" PRIMARY KEY ("id"),
    CONSTRAINT "FK_sale_product_tax_sale_product_id" FOREIGN KEY ("sale_product_id") REFERENCES "sale_product" ("id"),
    CONSTRAINT "FK_sale_product_tax_tax_id" FOREIGN KEY ("tax_id") REFERENCES "tax" ("id")
);
