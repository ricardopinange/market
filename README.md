## API configuration

The database used is PostgreSQL and the backup is in the market.sql file in the root folder.

To connect to the database, you must create the .env file in the root of the backend folder, being backend/.env with the following environment variables (the values must be updated according to your environment).

```bash
DB_CONNECTION=pgsql
DB_HOST=localhost
DB_PORT=5432
DB_DATABASE=postgres
DB_USERNAME=postgres
DB_PASSWORD=
```

The Backend must be run using the native PHP server using command php -S localhost:8080

The Frontend contains already compiled files.