<?php

use App\Http\Middleware\Route;
use App\Http\Middleware\Cors;

Cors::access();

Route::get('/api/product/list/{params}', 'ProductController@index');
Route::get('/api/product/edit/{id}', 'ProductController@edit');
Route::post('/api/product/insert', 'ProductController@store');
Route::post('/api/product/update/{id}', 'ProductController@update');
Route::delete('/api/product/delete/{id}', 'ProductController@destroy');

Route::get('/api/product-type/list/{params}', 'ProductTypeController@index');
Route::get('/api/product-type/edit/{id}', 'ProductTypeController@edit');
Route::post('/api/product-type/insert', 'ProductTypeController@store');
Route::post('/api/product-type/update/{id}', 'ProductTypeController@update');
Route::delete('/api/product-type/delete/{id}', 'ProductTypeController@destroy');

Route::get('/api/product-type-tax/list/{params}', 'ProductTypeTaxController@index');
Route::get('/api/product-type-tax/edit/{id}', 'ProductTypeTaxController@edit');
Route::post('/api/product-type-tax/insert', 'ProductTypeTaxController@store');
Route::post('/api/product-type-tax/update/{id}', 'ProductTypeTaxController@update');
Route::delete('/api/product-type-tax/delete/{id}', 'ProductTypeTaxController@destroy');

Route::get('/api/sale/list/{params}', 'SaleController@index');
Route::get('/api/sale/edit/{id}', 'SaleController@edit');
Route::post('/api/sale/insert', 'SaleController@store');
Route::post('/api/sale/update/{id}', 'SaleController@update');
Route::delete('/api/sale/delete/{id}', 'SaleController@destroy');

Route::get('/api/sale-product/list/{params}', 'SaleProductController@index');
Route::get('/api/sale-product/edit/{id}', 'SaleProductController@edit');
Route::post('/api/sale-product/insert', 'SaleProductController@store');
Route::post('/api/sale-product/update/{id}', 'SaleProductController@update');
Route::delete('/api/sale-product/delete/{id}', 'SaleProductController@destroy');

Route::get('/api/sale-product-tax/list/{params}', 'SaleProductTaxController@index');
Route::get('/api/sale-product-tax/edit/{id}', 'SaleProductTaxController@edit');
Route::post('/api/sale-product-tax/insert', 'SaleProductTaxController@store');
Route::post('/api/sale-product-tax/update/{id}', 'SaleProductTaxController@update');
Route::delete('/api/sale-product-tax/delete/{id}', 'SaleProductTaxController@destroy');

Route::get('/api/tax/list/{params}', 'TaxController@index');
Route::get('/api/tax/edit/{id}', 'TaxController@edit');
Route::post('/api/tax/insert', 'TaxController@store');
Route::post('/api/tax/update/{id}', 'TaxController@update');
Route::delete('/api/tax/delete/{id}', 'TaxController@destroy');
