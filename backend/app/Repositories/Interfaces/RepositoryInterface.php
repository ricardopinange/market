<?php
namespace App\Repositories\Interfaces;

Interface RepositoryInterface {

    public function all($request);
    public function store($data);
    public function find($id);
    public function update($data, $id);
    public function destroy($id);

}
