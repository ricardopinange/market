<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Product;
use App\Http\Middleware\Utils;

class ProductRepository implements RepositoryInterface
{
    public function all($request)
    {
        $params = Utils::getParams($request);
        $filters = [];

        if (!empty($params['product_type_id'])) {
            $filters[] = "product_type_id='{$params['product_type_id']}'";
        }

        return $this->findByFilter($filters);
    }

    public function store($request)
    {
        Utils::validate($request, Product::$fillable);

        if ($this->validate($request)) {
            $message = "Product already registered";
            throw new \Exception($message, 400);
        }

        $id = Product::create($request);
        return $this->find($id);
    }

    public function find($id)
    {
        return Product::find($id);
    }

    public function findByFilter(array $filter)
    {
        return Product::findByFilter($filter);
    }

    public function update($data, $id)
    {
        $register = $this->find($id);

        if (empty($register)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        if ($this->validate($data, $id)) {
            $message = "Product already registered";
            throw new \Exception($message, 400);
        }

        Product::update($data, $id);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = $this->find($id);

        if (empty($data)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        Product::delete($id);
    }

    public function validate(array $request, string $id = NULL)
    {
        $filters = [
            "name='" . addslashes(trim($request['name'])) . "'"
        ];

        if (isset($id)) {
            array_push($filters, "id != '{$id}'");
        }

        $data = $this->findByFilter($filters);

        return $data;
    }
}
