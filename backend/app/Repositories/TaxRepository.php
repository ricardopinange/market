<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Tax;
use App\Http\Middleware\Utils;

class TaxRepository implements RepositoryInterface
{
    public function all($request)
    {
        $params = Utils::getParams($request);
        $filters = [];

        if (!empty($params['name'])) {
            $filters[] = "name ILIKE '%{$params['name']}%'";
        }

        return $this->findByFilter($filters);
    }

    public function store($request)
    {
        Utils::validate($request, Tax::$fillable);

        if ($this->validate($request)) {
            $message = "Tax already registered";
            throw new \Exception($message, 400);
        }

        $id = Tax::create($request);
        return $this->find($id);
    }

    public function find($id)
    {
        return Tax::find($id);
    }

    public function findByFilter(array $filter)
    {
        return Tax::findByFilter($filter);
    }

    public function update($data, $id)
    {
        $register = $this->find($id);

        if (empty($register)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        if ($this->validate($data, $id)) {
            $message = "Tax already registered";
            throw new \Exception($message, 400);
        }

        Tax::update($data, $id);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = $this->find($id);

        if (empty($data)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        Tax::delete($id);
    }

    public function validate(array $request, string $id = NULL)
    {
        $filters = [
            "name='" . addslashes(trim($request['name'])) . "'"
        ];

        if (isset($id)) {
            array_push($filters, "id != '{$id}'");
        }

        $data = $this->findByFilter($filters);

        return $data;
    }
}
