<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\SaleProductTax;
use App\Http\Middleware\Utils;

class SaleProductTaxRepository implements RepositoryInterface
{
    public function all($request)
    {
        $params = Utils::getParams($request);
        $filters = [];

        if (!empty($params['sale_product_id'])) {
            $filters[] = "sale_product_id='{$params['sale_product_id']}'";
        }

        if (!empty($params['tax_id'])) {
            $filters[] = "tax_id='{$params['tax_id']}'";
        }

        return $this->findByFilter($filters);
    }

    public function store($request)
    {
        Utils::validate($request, SaleProductTax::$fillable);

        if ($this->validate($request)) {
            $message = "Sale Product Tax already registered";
            throw new \Exception($message, 400);
        }

        $id = SaleProductTax::create($request);
        return $this->find($id);
    }

    public function find($id)
    {
        return SaleProductTax::find($id);
    }

    public function findByFilter(array $filter)
    {
        return SaleProductTax::findByFilter($filter);
    }

    public function update($data, $id)
    {
        $register = $this->find($id);

        if (empty($register)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        if ($this->validate($data, $id)) {
            $message = "Sale Product Tax already registered";
            throw new \Exception($message, 400);
        }

        SaleProductTax::update($data, $id);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = $this->find($id);

        if (empty($data)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        SaleProductTax::delete($id);
    }

    public function validate(array $request, string $id = NULL)
    {
        $filters = [
            "sale_product_id='{$request['sale_product_id']}'",
            "tax_id='{$request['tax_id']}'"
        ];

        if (isset($id)) {
            array_push($filters, "id != '{$id}'");
        }

        $data = $this->findByFilter($filters);

        return $data;
    }
}
