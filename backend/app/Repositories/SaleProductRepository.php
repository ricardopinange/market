<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\SaleProduct;
use App\Http\Middleware\Utils;

class SaleProductRepository implements RepositoryInterface
{
    public function all($request)
    {
        $params = Utils::getParams($request);
        $filters = [];

        if (!empty($params['sale_id'])) {
            $filters[] = "sale_id='{$params['sale_id']}'";
        }

        if (!empty($params['product_id'])) {
            $filters[] = "product_id='{$params['product_id']}'";
        }

        return $this->findByFilter($filters);
    }

    public function store($request)
    {
        Utils::validate($request, SaleProduct::$fillable);

        if ($this->validate($request)) {
            $message = "Sale Product already registered";
            throw new \Exception($message, 400);
        }

        $id = SaleProduct::create($request);
        return $this->find($id);
    }

    public function find($id)
    {
        return SaleProduct::find($id);
    }

    public function findByFilter(array $filter)
    {
        return SaleProduct::findByFilter($filter);
    }

    public function update($data, $id)
    {
        $register = $this->find($id);

        if (empty($register)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        if ($this->validate($data, $id)) {
            $message = "Sale Product already registered";
            throw new \Exception($message, 400);
        }

        SaleProduct::update($data, $id);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = $this->find($id);

        if (empty($data)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        SaleProduct::delete($id);
    }

    public function validate(array $request, string $id = NULL)
    {
        $filters = [
            "sale_id='{$request['sale_id']}'",
            "product_id='{$request['product_id']}'"
        ];

        if (isset($id)) {
            array_push($filters, "id != '{$id}'");
        }

        $data = $this->findByFilter($filters);

        return $data;
    }
}
