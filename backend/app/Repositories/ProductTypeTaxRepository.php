<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\ProductTypeTax;
use App\Http\Middleware\Utils;

class ProductTypeTaxRepository implements RepositoryInterface
{
    public function all($request)
    {
        $params = Utils::getParams($request);
        $filters = [];

        if (!empty($params['product_type_id'])) {
            $filters[] = "product_type_id='{$params['product_type_id']}'";
        }

        if (!empty($params['tax_id'])) {
            $filters[] = "tax_id='{$params['tax_id']}'";
        }

        return $this->findByFilter($filters);
    }

    public function store($request)
    {
        Utils::validate($request, ProductTypeTax::$fillable);

        if ($this->validate($request)) {
            $message = "Product Type Tax already registered";
            throw new \Exception($message, 400);
        }

        $id = ProductTypeTax::create($request);
        return $this->find($id);
    }

    public function find($id)
    {
        return ProductTypeTax::find($id);
    }

    public function findByFilter(array $filter)
    {
        return ProductTypeTax::findByFilter($filter);
    }

    public function update($data, $id)
    {
        $register = $this->find($id);

        if (empty($register)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        if ($this->validate($data, $id)) {
            $message = "Product Type Tax already registered";
            throw new \Exception($message, 400);
        }

        ProductTypeTax::update($data, $id);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = $this->find($id);

        if (empty($data)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        ProductTypeTax::delete($id);
    }

    public function validate(array $request, string $id = NULL)
    {
        $filters = [
            "product_type_id='{$request['product_type_id']}'",
            "tax_id='{$request['tax_id']}'"
        ];

        if (isset($id)) {
            array_push($filters, "id != '{$id}'");
        }

        $data = $this->findByFilter($filters);

        return $data;
    }
}
