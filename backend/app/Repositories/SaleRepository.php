<?php

namespace App\Repositories;

use App\Repositories\Interfaces\RepositoryInterface;
use App\Models\Sale;
use App\Http\Middleware\Utils;

class SaleRepository implements RepositoryInterface
{
    public function all($request)
    {
        return Sale::select($request);
    }

    public function store($request)
    {
        Utils::validate($request, Sale::$fillable);
        $id = Sale::create($request);
        return $this->find($id);
    }

    public function find($id)
    {
        return Sale::find($id);
    }

    public function findByFilter(array $filter)
    {
        return Sale::findByFilter($filter);
    }

    public function update($data, $id)
    {
        $register = $this->find($id);

        if (empty($register)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        Sale::update($data, $id);
        return $this->find($id);
    }

    public function destroy($id)
    {
        $data = $this->find($id);

        if (empty($data)) {
            $message = "Register not found";
            throw new \Exception($message, 404);
        }

        Sale::delete($id);
    }

    public function validate(array $request, string $id = NULL)
    {
        return;
    }
}
