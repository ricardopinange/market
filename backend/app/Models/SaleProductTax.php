<?php

namespace App\Models;

Use PDO;
Use App\Http\Middleware\DatabaseConnection;
Use App\Http\Middleware\Utils;
Use App\Models\Model;

class SaleProductTax extends Model
{
    protected static $table = 'sale_product_tax';
    public static $fillable = [
        'sale_product_id' => 'required',
        'tax_id' => 'required',
        'percentage' => 'required',
        'amount' => 'required'
    ];

    public static function select($request)
    {
        return Model::select([
            'table' => self::$table,
            'columns' => '*',
            'orderBy' => 'sale_product_id,created_at'
        ]);
    }

    public static function create($request)
    {
        return Model::create([
            'table' => self::$table,
            'data' => Model::fillable($request, self::$fillable)
        ]);
    }

    public static function update($request, $id)
    {
        return Model::update([
            'table' => self::$table,
            'data' => Model::fillable($request, self::$fillable)
        ], $id);
    }

    public static function find($id)
    {
        return Model::find([
            'table' => self::$table,
            'columns' => '*',
            'id' => $id
        ]);
    }

    public static function findByFilter($request)
    {
        return Model::findByFilter([
            'table' => self::$table,
            'columns' => '*',
            'filter' => $request
        ]);
    }

    public static function delete($id)
    {
        return Model::delete([
            'table' => self::$table,
            'id' => $id
        ]);
    }

}
