<?php

namespace App\Models;

Use PDO;
Use App\Http\Middleware\DatabaseConnection;
Use App\Http\Middleware\Utils;

class Model
{
    public function __construct()
    {
    }

    public static function select($request)
    {
        try {

            $PDO = DatabaseConnection::PDO();
            $sql = $request['raw'] ??
                "SELECT {$request['columns']} " .
                "FROM {$request['table']} " .
                "WHERE deleted_at IS NULL " .
                "ORDER BY {$request['orderBy']}";
            $query = $PDO->prepare($sql);
            $query->execute();

            return $query->fetchAll();

        } catch(\PDOException $e) {
            throw $e;
        }
    }

    public static function create($request)
    {
        try {

            $data = Model::prepareInsert($request['data']);
            $PDO = DatabaseConnection::PDO();
            $sql = $request['raw'] ??
                "INSERT INTO {$request['table']} " .
                "(" . $data['columns'] . ") " .
                "VALUES (" . $data['values'] . ")";
            $query = $PDO->prepare($sql);
            $query->execute();

            return $data['id'];

        } catch(\PDOException $e) {
            throw $e;
        }
    }

    public static function update($request, $id)
    {
        try {

            $data = Model::prepareUpdate($request['data']);
            $PDO = DatabaseConnection::PDO();
            $sql = $request['raw'] ??
                "UPDATE {$request['table']} " .
                " SET " . $data['values'] .
                " WHERE id = :id";
            $query = $PDO->prepare($sql);
            $query->bindParam(':id', $id, PDO::PARAM_STR);
            $query->execute();

            return;

        } catch(\PDOException $e) {
            throw $e;
        }
    }

    public static function find($request)
    {
        try {

            $PDO = DatabaseConnection::PDO();
            $sql = $request['raw'] ??
                "SELECT {$request['columns']} " .
                "FROM {$request['table']} " .
                "WHERE deleted_at IS NULL " .
                "AND id = :id";
            $query = $PDO->prepare($sql);
            $query->bindParam(':id', $request['id'], PDO::PARAM_STR);
            $query->execute();

            return $query->fetch();

        } catch(\PDOException $e) {
            throw $e;
        }
    }

    public static function findByFilter($request)
    {
        try {

            $PDO = DatabaseConnection::PDO();
            $sql = $request['raw'] ??
                "SELECT {$request['columns']} " .
                "FROM {$request['table']} " .
                "WHERE deleted_at IS NULL";
            $filter = count($request['filter'])
                ? " AND " . implode(" AND ", $request['filter'])
                : "";
            $query = $PDO->prepare($sql.$filter);
            $query->execute();

            return $query->fetchAll();

        } catch(\PDOException $e) {
            throw $e;
        }
    }

    public static function delete($request)
    {
        try {

            $deleted_at = Utils::datetime();
            $PDO = DatabaseConnection::PDO();
            $sql = $request['raw'] ??
                "UPDATE {$request['table']} " .
                "SET deleted_at = :deleted_at " .
                "WHERE id = :id";
            $query = $PDO->prepare($sql);
            $query->bindParam(':id', $request['id'], PDO::PARAM_STR);
            $query->bindParam(':deleted_at', $deleted_at, PDO::PARAM_STR);
            $query->execute();

            return;

        } catch(\PDOException $e) {
            throw $e;
        }
    }

    public static function prepareInsert($array)
    {
        $fields = [];
        $columns = [];
        $values = [];
        $id = '';

        if (!empty($array)) {
            foreach ($array as $field => $value) {
                $$field = addslashes(trim($value ?? ''));
                $fields[$field][] = 1;
            }
        }

        foreach ($fields as $field => $value) {
            if ($$field == '0') $value = '0';
            else if ($$field == '' || $$field === NULL) $value = 'NULL';
            else $value = "'" . $$field . "'";

            $columns[] = $field;
            $values[] = $value;
        }

        $id = Utils::uuid();
        $columns[] = 'id';
        $values[] = "'" . $id . "'";
        $columns[] = 'created_at';
        $values[] = "'" . Utils::datetime() . "'";
        $columns[] = 'updated_at';
        $values[] = "'" . Utils::datetime() . "'";

        return [
            'columns' => implode(', ', $columns),
            'values' => implode(', ', $values),
            'id' => $id
        ];
    }

    public static function prepareUpdate($array)
    {
        $fields = [];
        $values = [];
        $id = '';

        if (!empty($array)) {
            foreach ($array as $field => $value) {
                $$field = addslashes(trim($value ?? ''));
                $fields[$field][] = 1;
            }
        }

        foreach ($fields as $field => $value) {
            if ($$field == '0') $value = '0';
            else if ($$field == '' || $$field === NULL) $value = 'NULL';
            else $value = "'" . $$field . "'";

            $values[] = $field . '=' . $value;
        }

        $values[] = "updated_at='" . Utils::datetime() . "'";

        return [
            'values' => implode(', ', $values)
        ];
    }

    public static function fillable($request, $fillable)
    {
        foreach ($request as $key => $value) {
            if (!array_key_exists($key, $fillable)) {
                unset($request[$key]);
            }
        }

        return $request;
    }

}
