<?php

namespace App\Models;

Use PDO;
Use App\Http\Middleware\DatabaseConnection;
Use App\Http\Middleware\Utils;
Use App\Models\Model;

class ProductTypeTax extends Model
{
    protected static $table = 'product_type_tax';
    public static $fillable = [
        'product_type_id' => 'required',
        'tax_id' => 'required',
        'percentage' => 'required'
    ];

    public static function select($request)
    {
        return Model::select([
            'table' => self::$table,
            'columns' => '*',
            'orderBy' => 'product_type_id,created_at'
        ]);
    }

    public static function create($request)
    {
        return Model::create([
            'table' => self::$table,
            'data' => Model::fillable($request, self::$fillable)
        ]);
    }

    public static function update($request, $id)
    {
        return Model::update([
            'table' => self::$table,
            'data' => Model::fillable($request, self::$fillable)
        ], $id);
    }

    public static function find($id)
    {
        return Model::find([
            'table' => self::$table,
            'columns' => '*',
            'id' => $id
        ]);
    }

    public static function findByFilter($request)
    {
        return Model::findByFilter([
            'table' => self::$table,
            'columns' => 'product_type_tax.*,
                          (SELECT name FROM tax WHERE id = product_type_tax.tax_id) tax_name',
            'filter' => $request
        ]);
    }

    public static function delete($id)
    {
        return Model::delete([
            'table' => self::$table,
            'id' => $id
        ]);
    }

}
