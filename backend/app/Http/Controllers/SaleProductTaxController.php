<?php

namespace App\Http\Controllers;

use App\Repositories\SaleProductTaxRepository;

class SaleProductTaxController extends Controller
{
    private $saleProductTaxRepository;

    public function __construct()
    {
        $this->saleProductTaxRepository = new SaleProductTaxRepository;
    }

    public function index($request)
    {
        try {
            $data = $this->saleProductTaxRepository->all($request);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function store($request)
    {
        try {
            $data = $this->saleProductTaxRepository->store($request->getData());
            return $this->responseSuccess($data, 'Sale Product Tax successfully added');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function edit($request)
    {
        try {
            $data = $this->saleProductTaxRepository->find($request->params[0]);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function update($request)
    {
        try {
            $data = $this->saleProductTaxRepository->update($request->getData(), $request->params[0]);
            return $this->responseSuccess($data, 'Sale Product Tax successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function destroy($request)
    {
        try {
            $this->saleProductTaxRepository->destroy($request->params[0]);
            return $this->responseSuccess([], 'Sale Product Tax successfully deleted');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }
}
