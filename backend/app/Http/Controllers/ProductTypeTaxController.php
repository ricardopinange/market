<?php

namespace App\Http\Controllers;

use App\Repositories\ProductTypeTaxRepository;

class ProductTypeTaxController extends Controller
{
    private $productTypeTaxRepository;

    public function __construct()
    {
        $this->productTypeTaxRepository = new ProductTypeTaxRepository;
    }

    public function index($request)
    {
        try {
            $data = $this->productTypeTaxRepository->all($request);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function store($request)
    {
        try {
            $data = $this->productTypeTaxRepository->store($request->getData());
            return $this->responseSuccess($data, 'Product Type Tax successfully added');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function edit($request)
    {
        try {
            $data = $this->productTypeTaxRepository->find($request->params[0]);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function update($request)
    {
        try {
            $data = $this->productTypeTaxRepository->update($request->getData(), $request->params[0]);
            return $this->responseSuccess($data, 'Product Type Tax successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function destroy($request)
    {
        try {
            $this->productTypeTaxRepository->destroy($request->params[0]);
            return $this->responseSuccess([], 'Product Type Tax successfully deleted');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }
}
