<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepository;

class ProductController extends Controller
{
    private $productRepository;

    public function __construct()
    {
        $this->productRepository = new ProductRepository;
    }

    public function index($request)
    {
        try {
            $data = $this->productRepository->all($request);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function store($request)
    {
        try {
            $data = $this->productRepository->store($request->getData());
            return $this->responseSuccess($data, 'Product successfully added');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function edit($request)
    {
        try {
            $data = $this->productRepository->find($request->params[0]);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function update($request)
    {
        try {
            $data = $this->productRepository->update($request->getData(), $request->params[0]);
            return $this->responseSuccess($data, 'Product successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function destroy($request)
    {
        try {
            $this->productRepository->destroy($request->params[0]);
            return $this->responseSuccess([], 'Product successfully deleted');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }
}
