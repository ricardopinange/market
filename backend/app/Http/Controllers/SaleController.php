<?php

namespace App\Http\Controllers;

use App\Repositories\SaleRepository;

class SaleController extends Controller
{
    private $saleRepository;

    public function __construct()
    {
        $this->saleRepository = new SaleRepository;
    }

    public function index($request)
    {
        try {
            $data = $this->saleRepository->all($request);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function store($request)
    {
        try {
            $data = $this->saleRepository->store($request->getData());
            return $this->responseSuccess($data, 'Sale successfully added');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function edit($request)
    {
        try {
            $data = $this->saleRepository->find($request->params[0]);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function update($request)
    {
        try {
            $data = $this->saleRepository->update($request->getData(), $request->params[0]);
            return $this->responseSuccess($data, 'Sale successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function destroy($request)
    {
        try {
            $this->saleRepository->destroy($request->params[0]);
            return $this->responseSuccess([], 'Sale successfully deleted');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }
}
