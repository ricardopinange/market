<?php

namespace App\Http\Controllers;

use App\Repositories\ProductTypeRepository;

class ProductTypeController extends Controller
{
    private $productTypeRepository;

    public function __construct()
    {
        $this->productTypeRepository = new ProductTypeRepository;
    }

    public function index($request)
    {
        try {
            $data = $this->productTypeRepository->all($request);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function store($request)
    {
        try {
            $data = $this->productTypeRepository->store($request->getData());
            return $this->responseSuccess($data, 'Product Type successfully added');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function edit($request)
    {
        try {
            $data = $this->productTypeRepository->find($request->params[0]);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function update($request)
    {
        try {
            $data = $this->productTypeRepository->update($request->getData(), $request->params[0]);
            return $this->responseSuccess($data, 'Product Type successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function destroy($request)
    {
        try {
            $this->productTypeRepository->destroy($request->params[0]);
            return $this->responseSuccess([], 'Product Type successfully deleted');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }
}
