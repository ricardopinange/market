<?php

namespace App\Http\Controllers;

Use App\Http\Middleware\Response;

class Controller
{
    /**
     * Is http response valid?
     */
    public function isValidHttpResponse($code)
    {
        $intCode = intval($code);
        return $intCode >= 100 && $intCode < 600 && is_int($intCode);
    }

    /**
     * Display request success data.
     */
    public function responseSuccess($data, $message = true)
    {
        return Response::json(
            [
                'success' => true,
                'message' => $message,
                'data' => $data
            ],
            200
        );
    }

    /**
     * Display request error data.
     */
    public function responseError($e, $message = false)
    {
        error_log('Exception: ' . $e->getMessage());
        return Response::json(
            [
                'error' => true,
                'message' => $message,
                'data' => json_decode($e->getMessage()) &&
                          json_last_error() === JSON_ERROR_NONE
                          ? json_decode($e->getMessage())
                          : $e->getMessage()
            ],
            $this->isValidHttpResponse($e->getCode())
                ? $e->getCode()
                : 500
        );
    }
}
