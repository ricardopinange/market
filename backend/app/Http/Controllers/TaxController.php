<?php

namespace App\Http\Controllers;

use App\Repositories\TaxRepository;

class TaxController extends Controller
{
    private $taxRepository;

    public function __construct()
    {
        $this->taxRepository = new TaxRepository;
    }

    public function index($request)
    {
        try {
            $data = $this->taxRepository->all($request);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function store($request)
    {
        try {
            $data = $this->taxRepository->store($request->getData());
            return $this->responseSuccess($data, 'Tax successfully added');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function edit($request)
    {
        try {
            $data = $this->taxRepository->find($request->params[0]);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function update($request)
    {
        try {
            $data = $this->taxRepository->update($request->getData(), $request->params[0]);
            return $this->responseSuccess($data, 'Tax successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function destroy($request)
    {
        try {
            $this->taxRepository->destroy($request->params[0]);
            return $this->responseSuccess([], 'Tax successfully deleted');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }
}
