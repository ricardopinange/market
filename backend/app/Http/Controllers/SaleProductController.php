<?php

namespace App\Http\Controllers;

use App\Repositories\SaleProductRepository;

class SaleProductController extends Controller
{
    private $saleProductRepository;

    public function __construct()
    {
        $this->saleProductRepository = new SaleProductRepository;
    }

    public function index($request)
    {
        try {
            $data = $this->saleProductRepository->all($request);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function store($request)
    {
        try {
            $data = $this->saleProductRepository->store($request->getData());
            return $this->responseSuccess($data, 'Sale Product successfully added');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function edit($request)
    {
        try {
            $data = $this->saleProductRepository->find($request->params[0]);
            return $this->responseSuccess($data);
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function update($request)
    {
        try {
            $data = $this->saleProductRepository->update($request->getData(), $request->params[0]);
            return $this->responseSuccess($data, 'Sale Product successfully updated');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }

    public function destroy($request)
    {
        try {
            $this->saleProductRepository->destroy($request->params[0]);
            return $this->responseSuccess([], 'Sale Product successfully deleted');
        } catch(\Exception $e) {
            return $this->responseError($e);
        }
    }
}
