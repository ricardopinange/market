<?php

namespace App\Http\Middleware;

class Request
{
    public $params;
    public $method;
    public $content_type;

    public function __construct($params = [])
    {
        $this->params = $params;
        $this->method = trim($_SERVER['REQUEST_METHOD']);
        $this->content_type = !empty($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
    }

    public function getBody()
    {
        if ($this->method !== 'POST') {
            return '';
        }

        $body = [];
        foreach ($_POST as $key => $value) {
            $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
        }

        return $body;
    }

    public function getJSON()
    {
        if ($this->method !== 'POST') {
            return [];
        }

        if (strcasecmp($this->content_type, 'application/json') !== 0) {
            return [];
        }

        $content = trim(file_get_contents("php://input"));
        $json_decoded = json_decode($content, true);

        return $json_decoded;
    }

    public function getData()
    {
        return array_merge($this->getBody(), $this->getJSON());
    }
}