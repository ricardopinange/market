<?php

namespace App\Http\Middleware;

use PDO;

class DatabaseConnection
{

    public static $singletonPDO;
    public $PDO;

    public function connectPDO()
    {
        $env = parse_ini_file("{$_SERVER['DOCUMENT_ROOT']}/.env");
        $this->PDO = new PDO("{$env['DB_CONNECTION']}:host={$env['DB_HOST']};port={$env['DB_PORT']};dbname={$env['DB_DATABASE']}",$env['DB_USERNAME'],$env['DB_PASSWORD'],[]);
        $this->PDO->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
        $this->PDO->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $this->PDO;
    }

    public static function PDO()
    {

        if (self::$singletonPDO):
            return self::$singletonPDO;
        else:
            return self::$singletonPDO = (new self)->connectPDO();
        endif;

    }

}

?>