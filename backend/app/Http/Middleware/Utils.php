<?php

namespace App\Http\Middleware;

class Utils
{
    public static function datetime()
    {
        return date("Y-m-d H:i:s");
    }

    public static function uuid($data = null)
    {
        $data = $data ?? random_bytes(16);
        assert(strlen($data) == 16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

    public static function validate($request, $validate)
    {
        $errors = [];

        foreach ((array)$validate as $field => $value) {
            $validations = explode('|', $value);

            foreach ($validations as $key => $value_param) {
                $param = explode(':', $value_param);

                if ($param[0] == 'required' && !array_key_exists($field, $request)) {
                    $errors[$field][] = "The {$field} field is required.";
                }

                if ($param[0] == 'min' && !empty($request[$field]) && strlen($request[$field]) < $param[1]) {
                    $errors[$field][] = "The {$field} field must be at least {$param[1]} characters.";
                }

                if ($param[0] == 'max' && !empty($request[$field]) && strlen($request[$field]) > $param[1]) {
                    $errors[$field][] = "The {$field} field must not be greater than {$param[1]} characters.";
                }
            }
        }

        if (count($errors)) {
            $message = json_encode($errors);
            throw new \Exception($message, 200);
        }
    }

    public static function getParams($http_query)
    {
        parse_str(str_replace('?','', $http_query->params[0]), $params);
        return $params;
    }

}
