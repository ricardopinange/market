<?php

namespace App\Http\Middleware;

class Route
{

    public static function get($route, $callback)
    {
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'GET') !== 0) {
            return;
        }

        self::on($route, $callback);
    }

    public static function post($route, $callback)
    {
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'POST') !== 0) {
            return;
        }

        self::on($route, $callback);
    }

    public static function put($route, $callback)
    {
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'PUT') !== 0) {
            return;
        }

        self::on($route, $callback);
    }

    public static function delete($route, $callback)
    {
        if (strcasecmp($_SERVER['REQUEST_METHOD'], 'DELETE') !== 0) {
            return;
        }

        self::on($route, $callback);
    }

    public static function on($exprr, $callback)
    {
        
        $params = $_SERVER['REQUEST_URI'];
        $params = (stripos($params, "/") !== 0) ? "/" . $params : $params;
        $search = ['/', '{params}', '{id}'];
        $replace = ['\/', '(.*?)', '([a-z0-9\-]*)'];
        $exprr = str_replace($search, $replace, $exprr);
        $matched = preg_match('/^' . ($exprr) . '$/', $params, $is_matched, PREG_OFFSET_CAPTURE);

        if ($matched) {
            array_shift($is_matched);

            $params = array_map(function ($param) {
                return $param[0];
            }, $is_matched);

            [$controller, $method] = explode('@', $callback);
            $class = "\App\Http\Controllers\\{$controller}";
            (new $class)->{$method}(new Request($params), new Response());
        }
    }

}